package com.example.b1nfctool;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;

import android.net.Uri;
import android.os.Bundle;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import android.nfc.NfcAdapter;
import android.nfc.Tag;

import nfc.TagDiscovery;
import nfc.TagDiscovery.onTagDiscoveryCompletedListener;

import com.st.st25sdk.Helper;
import com.st.st25sdk.NFCTag;
import com.st.st25sdk.STException;
import com.st.st25sdk.TagCache;
import com.st.st25sdk.TagHelper;
import com.st.st25sdk.ndef.NDEFRecord;
import com.st.st25sdk.type5.STType5PasswordInterface;
import com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration;
import com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag;


import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import static com.st.st25sdk.TagHelper.ReadWriteProtection;
import static com.st.st25sdk.TagHelper.ReadWriteProtection.READABLE_AND_WRITABLE;
import static com.st.st25sdk.TagHelper.ReadWriteProtection.READABLE_AND_WRITE_PROTECTED_BY_PWD;
import static com.st.st25sdk.TagHelper.ReadWriteProtection.READ_AND_WRITE_PROTECTED_BY_PWD;
import static com.st.st25sdk.TagHelper.ReadWriteProtection.READ_PROTECTED_BY_PWD_AND_WRITE_IMPOSSIBLE;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement.FULL_DUPLEX;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement.PWM_FREQ_REDUCED;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement.PWM_FREQ_REDUCED_AND_ONE_QUARTER_FULL_POWER_WHILE_RF_CMD;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement.PWM_IN_HZ_WHILE_RF_CMD;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement.PWM_ONE_QUARTER_FULL_POWER_WHILE_RF_CMD;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.DualityManagement;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming.HALF_FULL_POWER;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming.ONE_QUARTER_FULL_POWER;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming.THREE_QUARTER_FULL_POWER;
import static com.st.st25sdk.type5.st25dvpwm.ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming.FULL_POWER;
import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.PWM1;
import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.PWM2;


public class MainActivity extends AppCompatActivity
        implements onTagDiscoveryCompletedListener
{
    private int set_current = 0;    //holds current value to be set up on ECG in mA, not valid 0 at start
    String driver_in_action;           //selected model to be processed
    //public Resources res = getResources();
    //public String[] ecg_models = res.getStringArray(R.array.b1_models);// prepare list of ecg  models
    private TextView currentField;     //text area to display ECG current value in mA
    private EditText inputCurrent;    //area for manual (keypad) input of current value
    private TextView NfcWarningField;
    private Button EnableNfcButton;
    private TextView ProceedECG;
    private NfcAdapter NfcAdapter; //android stuff
    private PendingIntent PendingIntent;
    static private NFCTag Tag;     //ST lib general tag
    private ST25DVPwmTag B1Tag;    //particular tag model used in B1 ECGs
    private STType5PasswordInterface mSTType5PasswordInterface;
    private boolean pwd_NOT_OK = true;
    final private byte[] passwordCFGPWMdefault ={0,0,0,0};
    final private byte[] passwordCFGB1 ={(byte)0xCF, (byte)0xA5, (byte)0x5A, (byte)0xB1};   //private CFG pass hardcoded yet
    final private byte[] passwordPWMB1 ={(byte)0xBC, (byte)0xEC, (byte)0x55, (byte)0xB1};   //private PWM pass hardcoded yet
    //obtain data from threads: manage CFG&PWM passwords
    //testing default/custom passwords
    private final int DEFAULT_CFG_PWD_OK = 0;      // успех операции проверки пароля по умолчанию (заводские настройки для CFG пароля)
    private final int DEFAULT_CFG_PWD_NOT_OK = 1;  // заводской пароль не подошел (пароль CFG изменен)
    private final int B1_CFG_PWD_OK = 2;           // успех операции проверки частного пароля CFG
    private final int B1_CFG_PWD_NOT_OK = 3;       // частный пароль CFG не подошел (метка далее недоступна для работы)
    private final int DEFAULT_PWM_PWD_OK = 4;      // заводской пароль для PWM подтвержден
    private final int DEFAULT_PWM_PWD_NOT_OK = 5;  // заводской пароль для PWM изменен
    private final int B1_PWM_PWD_OK = 6;           // успех операции проверки частного пароля PWM
    private final int B1_PWM_PWD_NOT_OK = 7;       // частный пароль PWM не подошел (метка далее недоступна для работы)
    //setting custom passwords
    private final int B1_CFG_PWD_SET_OK = 8;       // CFG пароль установлен успешно
    private final int B1_CFG_PWD_SET_FAIL = 9;     // ошибка установки CFG пароля
    private final int B1_PWM_PWD_SET_OK = 10;      // PWM пароль установлен успешно
    private final int B1_PWM_PWD_SET_FAIL = 11;    // ошибка установки PWM пароля
    //general
    private final int PWM_SETUP_DONE = 12;         // ШИМ сконфигурирован под требования приложения
    private final int COMMAND_FAILED = 13;         // общая ошибка операции с паролем
    private final int PRESENT_CFG_PWD_FAILED = 14; // предоставлен неверный CFG пароль
    private final int PRESENT_PWM_PWD_FAILED = 15; // предоставлен неверный PWM пароль
    private final int NOT_IN_FIELD = 16;           // метка вне области ридера
    private final int NDA_FAIL = 17;               // опция защищена NDA
    private  final int PRESENT_CFG_PWD = 18;       // запрошен CFG пароль
    private  final int PRESENT_PWM_PWD = 19;       // запрошен PWM пароль
    private final int PWM_SETUP_FAIL = 20;         // ошибка конфигурации ШИМ

    public MainActivity()
    {
        if (BuildConfig.DEBUG)
        {
            Run_B1_NFC_tool();
        }
    }
    @Override
    //start GUI
      protected void onCreate(Bundle savedInstanceState)
       {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  //setup visual elements
        Resources res = getResources();
        //allow user to select the ECG model
        String[] ecg_models = res.getStringArray(R.array.b1_models);// prepare list of ecg  models
        driver_in_action = ecg_models[0]; //and set default one (no selection yet)
        Button cur120 = (Button)findViewById(R.id.but120);           //prepare predefined current settings
        Button cur250 = (Button)findViewById(R.id.but250);
        Button cur300 = (Button)findViewById(R.id.but300);
        Button cur350 = (Button)findViewById(R.id.but350);
        Button cur500 = (Button)findViewById(R.id.but500);
        Button cur600 = (Button)findViewById(R.id.but600);
        Button cur700 = (Button)findViewById(R.id.but700);
        Button cur1050 = (Button)findViewById(R.id.but1050);
        TextView esg_selection = (TextView) findViewById(R.id.text_ecg_chosen);//display list of models
        Spinner spinner = (Spinner) findViewById(R.id.spin_select_ecg);
        NfcWarningField = (TextView) findViewById(R.id.nfcWarning);
        ProceedECG =  (TextView) findViewById(R.id.text_attach_ecg);
        EnableNfcButton = (Button) findViewById(R.id.enableNfcButton);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ecg_models);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        // Применяем адаптер к элементу spinner
        spinner.setAdapter(adapter);
        //Обработкик выбранного элемента spinner
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener()
            {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
               {// Получаем выбранный объект
                   driver_in_action = (String)parent.getItemAtPosition(position);
                   esg_selection.setText(driver_in_action);
                //now enable current presets valid for selected model
                   switch (driver_in_action)
                   {
                       case "B1x25W": case "B1x50W":
                           cur120.setVisibility(View.VISIBLE);
                           cur250.setVisibility(View.VISIBLE);
                           cur300.setVisibility(View.VISIBLE);
                           cur350.setVisibility(View.VISIBLE);
                           cur500.setVisibility(View.INVISIBLE);
                           cur600.setVisibility(View.INVISIBLE);
                           cur700.setVisibility(View.INVISIBLE);
                           cur1050.setVisibility(View.INVISIBLE);
                           set_current = 350;
                           break;
                       case "B1x80W":
                           cur120.setVisibility(View.VISIBLE);
                           cur250.setVisibility(View.VISIBLE);
                           cur300.setVisibility(View.VISIBLE);
                           cur350.setVisibility(View.VISIBLE);
                           cur500.setVisibility(View.VISIBLE);
                           cur600.setVisibility(View.VISIBLE);
                           cur700.setVisibility(View.VISIBLE);
                           cur1050.setVisibility(View.INVISIBLE);
                           set_current = 350;
                           break;
                       case "RC80W": case "RC120W":
                           cur120.setVisibility(View.INVISIBLE);
                           cur250.setVisibility(View.INVISIBLE);
                           cur300.setVisibility(View.INVISIBLE);
                           cur350.setVisibility(View.INVISIBLE);
                           cur500.setVisibility(View.INVISIBLE);
                           cur600.setVisibility(View.INVISIBLE);
                           cur700.setVisibility(View.VISIBLE);
                           cur1050.setVisibility(View.VISIBLE);
                           set_current = 1050;
                           break;
                       case "BI20W": case "BI40W":
                           cur120.setVisibility(View.INVISIBLE);
                           cur250.setVisibility(View.INVISIBLE);
                           cur300.setVisibility(View.INVISIBLE);
                           cur350.setVisibility(View.VISIBLE);
                           cur500.setVisibility(View.VISIBLE);
                           cur600.setVisibility(View.INVISIBLE);
                           cur700.setVisibility(View.VISIBLE);
                           cur1050.setVisibility(View.INVISIBLE);
                           set_current = 350;
                           break;
                       default: //no current presets available at start
                           cur120.setVisibility(View.INVISIBLE);
                           cur250.setVisibility(View.INVISIBLE);
                           cur300.setVisibility(View.INVISIBLE);
                           cur350.setVisibility(View.INVISIBLE);
                           cur500.setVisibility(View.INVISIBLE);
                           cur600.setVisibility(View.INVISIBLE);
                           cur700.setVisibility(View.INVISIBLE);
                           cur1050.setVisibility(View.INVISIBLE);
                           set_current = 0;
                   }
                   set_current = filter_current(set_current); //filter manual input in conjunction with selected model
                   show_current();
               }
               @Override
               public void onNothingSelected(AdapterView<?> parent) { }
            };
        spinner.setOnItemSelectedListener(itemSelectedListener);
        //show default current in both static and editable areas
        currentField = (TextView)findViewById(R.id.textCrntSet);
        currentField.setText(Integer.toString(set_current) + "mA");
        inputCurrent = (EditText) findViewById(R.id.inputCrntSet);//enable manual current input
        inputCurrent.setText(Integer.toString(set_current));
           //prepare NFC stuff
        NfcAdapter = NfcAdapter.getDefaultAdapter(this);
        PendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
       }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }
    @Override
      public void onPause()
       {
        super.onPause();
        if (NfcAdapter != null)
          {
            try
              {
                NfcAdapter.disableForegroundDispatch(this);
              //  Log.v(TAG, "disableForegroundDispatch");
              }
            catch (IllegalStateException e)
              {
             //   Log.w(TAG, "Illegal State Exception disabling NFC. Assuming application is terminating.");
              }
            catch (UnsupportedOperationException e)
              {
              //  Log.w(TAG, "FEATURE_NFC is unavailable.");
              }
          }
      }
    @Override
      public void onResume()
       {
        Intent intent = getIntent();
      //  Log.d(TAG, "Resume mainActivity intent: " + intent);
        super.onResume();
        if (NfcAdapter != null)
          {
          //  Log.v(TAG, "enableForegroundDispatch");
            NfcAdapter.enableForegroundDispatch(this, PendingIntent, null /*nfcFiltersArray*/, null /*nfcTechLists*/);
            if (NfcAdapter.isEnabled())
              {// NFC enabled
                NfcWarningField.setVisibility(View.INVISIBLE);
                EnableNfcButton.setVisibility(View.INVISIBLE);
                ProceedECG.setVisibility(View.VISIBLE);
                if (driver_in_action.equals("Model")) //no driver selected yet
                {
                    ProceedECG.setText(R.string.select_ecg);
                } else
                    {
                    ProceedECG.setText(R.string.proceed_ecg);
                    }
              }
            else
              {// NFC disabled
                NfcWarningField.setText(R.string.nfc_currently_disabled);
                NfcWarningField.setVisibility(View.VISIBLE);
                EnableNfcButton.setVisibility(View.VISIBLE);
                ProceedECG.setVisibility(View.INVISIBLE);
              }
          }
        else
            {// NFC not available on this phone!!!
            NfcWarningField.setText(R.string.nfc_not_available);
            NfcWarningField.setVisibility(View.VISIBLE);
            EnableNfcButton.setVisibility(View.INVISIBLE);
            ProceedECG.setVisibility(View.INVISIBLE);
            }
        processIntent(intent);
    }

    //to catch up NFC event
    @SuppressLint("MissingSuperCall")
    @Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
      //  Log.d(TAG, "onNewIntent " + intent);
        setIntent(intent);
    }
//perform tag discovery in general
    void processIntent(Intent intent)
    {
        if(intent == null) {return; }
    //    Log.d(TAG, "processIntent " + intent);
        Tag androidTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (androidTag != null)
          {// A tag has been taped
            // Perform tag discovery in an asynchronous task
            // onTagDiscoveryCompleted() will be called when the discovery is completed.
           new TagDiscovery(this).execute(androidTag);
            // This intent has been processed. Reset it to be sure that we don't process it again
            // if the MainActivity is resumed
            setIntent(null);
        } else
        {
        }
    }

    //here after tag being read
    @Override
    public void onTagDiscoveryCompleted(NFCTag nfcTag, TagHelper.ProductID productId, STException e)
    {
        Toast.makeText(getApplication(),"Метка прочитана: " + productId.toString(), Toast.LENGTH_LONG).show();
        if (e != null)
          {
            Toast.makeText(getApplication(), R.string.error_while_reading_the_tag, Toast.LENGTH_LONG).show();
            return;
          }
        Tag = nfcTag;
        B1Tag = (ST25DVPwmTag)nfcTag; //in order to use particular tag functions
        switch (productId)
        {
            case PRODUCT_ST_ST25DV02K_W1:
            case PRODUCT_ST_ST25DV02K_W2:
                Run_pwd_check(); // proceed with updating ECG current only with password enabled tag
                break;
            default:// not supported tag found
                Toast.makeText(getApplication(), getResources().getString(R.string.unknown_tag), Toast.LENGTH_LONG).show();
                break;
        }
    }

    //make discovered tag of interest (ST25DV02Kxx) your own:
    //change configuration and PWM passwords if possible
    // Present default configuration password (0x00000000) - open secure session
    private void  Run_pwd_check()
    {// check if the tag doesn't implement a STType5PasswordInterface
        try {
            mSTType5PasswordInterface = (STType5PasswordInterface) Tag;
            } catch (ClassCastException e) {// Tag not implementing STType5PasswordInterface
            Toast.makeText(getApplication(), getResources().getString(R.string.no_pwd_support), Toast.LENGTH_LONG).show();
           }
      //get configuration password ID
        /*
        try {
             passwordNumber = ((STType5PasswordInterface) Tag).getConfigurationPasswordNumber();
            } catch (STException e) {
            Toast.makeText(getApplication(),"Pwd ID failed", Toast.LENGTH_SHORT).show();
           }
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_CONFIGURATION_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.presentPassword(passwordID, passwordCFGPWMdefault);
                    handler.sendEmptyMessage(DEFAULT_CFG_PWD_OK);  // Default password presented successfully
                    }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        default:
                            handler.sendEmptyMessage(DEFAULT_CFG_PWD_NOT_OK);
                            break;
                    }
                }
            }
        }).start();
    }
    //Change configuration password in order to be able to set password for PWMs
    private void setB1pwdCFG()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_CONFIGURATION_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.writePassword(passwordID, passwordCFGB1);
                    handler.sendEmptyMessage(B1_CFG_PWD_SET_OK);  // B1 password set successfully
                    }
                catch (STException e) {// Change password failed
                    switch (e.getError())
                    {
                     case TAG_NOT_IN_THE_FIELD:
                          handler.sendEmptyMessage(NOT_IN_FIELD);
                          break;
                     default:
                          handler.sendEmptyMessage(B1_CFG_PWD_SET_FAIL);
                          break;
                    }
                }
            }
        }).start();
    }
    //check if current CFG pass is our. If not - we should not proceed with this tag
    private void checkB1pwdCFG()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_CONFIGURATION_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.presentPassword(passwordID, passwordCFGB1);
                    handler.sendEmptyMessage(B1_CFG_PWD_OK);  // Custom password presented successfull
                }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        default:
                            handler.sendEmptyMessage(B1_CFG_PWD_NOT_OK);
                            break;
                    }
                }
            }
        }).start();
    }
    //check if default PWM pass is valid
    private void check0pwdPWM()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_PWM_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.presentPassword(passwordID, passwordCFGPWMdefault);
                    handler.sendEmptyMessage(DEFAULT_PWM_PWD_OK);  // Custom password presented successfull
                }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        default:
                            handler.sendEmptyMessage(DEFAULT_PWM_PWD_NOT_OK);
                            break;
                    }
                }
            }
        }).start();
    }
    //write custom PWM pass. Old (default) pwm pass was presented right before calling this method
    private void setB1pwdPWM()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_PWM_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.writePassword(passwordID, passwordPWMB1);
                    handler.sendEmptyMessage(B1_PWM_PWD_SET_OK);  // Custom password presented successfull
                }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        case ISO15693_BLOCK_IS_LOCKED:
                            handler.sendEmptyMessage(PRESENT_PWM_PWD); //just for debug. PWM password provided prior calling this method
                            break;
                        default:
                            handler.sendEmptyMessage(B1_PWM_PWD_SET_FAIL);
                            break;
                    }
                }
            }
        }).start();
    }
    //check if custom PWM pass is valid
    private void checkB1pwdPWM()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_PWM_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.presentPassword(passwordID, passwordPWMB1);
                    handler.sendEmptyMessage(B1_PWM_PWD_OK);  // Custom password presented successfull
                }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        default:
                            handler.sendEmptyMessage(B1_PWM_PWD_NOT_OK);
                            break;
                    }
                }
            }
        }).start();
    }
    //update PWM settings right after presenting default CFG pass
    private void updatePWMsettings()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {// Read current PWM Config
                    ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming currentPwm1Trimming = B1Tag.getPwmOutputDriverTrimming(PWM1);
                    ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming currentPwm2Trimming = (B1Tag.getNumberOfPwm() == 2) ?  B1Tag.getPwmOutputDriverTrimming(PWM2) : null;
                    ST25DV02KWRegisterPwmRfConfiguration.DualityManagement currentDualityManagement = B1Tag.getDualityManagement();
                    TagHelper.ReadWriteProtection currentPwmReadWriteProtection = B1Tag.getPwmCtrlAccessRights();
                    // Default PWM settings
                    ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming newPwm1Trimming = FULL_POWER;
                    ST25DV02KWRegisterPwmRfConfiguration.OutputDriverTrimming newPwm2Trimming = ONE_QUARTER_FULL_POWER;
                    ST25DV02KWRegisterPwmRfConfiguration.DualityManagement newDualityManagement = PWM_FREQ_REDUCED_AND_ONE_QUARTER_FULL_POWER_WHILE_RF_CMD;
                    TagHelper.ReadWriteProtection newPwmReadWriteProtection = READABLE_AND_WRITE_PROTECTED_BY_PWD;
                    // Update the tag with the values that have to be changed
                    if (currentPwm1Trimming != newPwm1Trimming) {
                        B1Tag.setPwmOutputDriverTrimming(PWM1, newPwm1Trimming);
                    }
                    if ((currentPwm2Trimming != newPwm2Trimming) && (B1Tag.getNumberOfPwm() == 2)) {
                        B1Tag.setPwmOutputDriverTrimming(PWM2, newPwm2Trimming);
                    }
                    if (currentDualityManagement != newDualityManagement) {
                        B1Tag.setDualityManagement(newDualityManagement);
                    }
                    if (currentPwmReadWriteProtection != newPwmReadWriteProtection) {
                        B1Tag.setPwmCtrlAccessRights(newPwmReadWriteProtection);
                    }
                    handler.sendEmptyMessage(PWM_SETUP_DONE);
                } catch (STException e) {
                    switch (e.getError()) {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        case CONFIG_PASSWORD_NEEDED:
                            handler.sendEmptyMessage(PRESENT_CFG_PWD);  //just for debug. CFG password provided prior calling this method
                            break;
                        default:
                            handler.sendEmptyMessage(PWM_SETUP_FAIL);
                    }
                }
            }
        }).start();
    }

    //launch update of B1 ECG current upon correct tag recognition automatically or wait for user
    // until ECG model is selected
    private void startTagActivity()
    {// We are about to start the activity related to a tag so mTag should be non null
        if(getTag() == null) return;
     // We cannot proceed with tag if it has no password protection or password is unknown
        if (pwd_NOT_OK)
        {
            Toast.makeText(getApplication(), getResources().getString(R.string.cfg_err), Toast.LENGTH_LONG).show();
            return;
        }
        ProceedECG.setVisibility(View.VISIBLE);
        if (driver_in_action.equals("Model")) //no driver selected while tag being detected
        {
            ProceedECG.setText(R.string.select_ecg);
            return;
        }
        Intent intent = new Intent(this, B1configActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Flag indicating that we are displaying the information of a new tag
        intent.putExtra("ecg_name", driver_in_action);
        intent.putExtra("ecg_crnt", set_current);
        intent.putExtra("pwm_pwd", passwordPWMB1);
        startActivity(intent);
    }
    //pass discovered tags to process in config activity
    static public NFCTag getTag()
    {
        return Tag;
    }
    //open NFC settings
      public void enable_nfc(View view)
      {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
          {
              Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
              startActivity(intent);
          }
          else
          {
              Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
              startActivity(intent);
          }
      }
    //current manual entry
    public void input_current(View view)
    {
       try
          {
              set_current = Integer.parseInt(inputCurrent.getText().toString());
          }
       catch (NumberFormatException nfe)
          {
              set_current = 350;
          }
        set_current = filter_current(set_current); //filter manual input in conjunction with selected model
        show_current();
        startTagActivity();   //tag activity starts upon current selection
    }
    //predefined current entry
    public void defined_current_set(View view)
    {
        Button button = (Button)view;
        try
        {
            set_current = Integer.parseInt(button.getText().toString());
        }
        catch (NumberFormatException nfe)
        {
            set_current = 350;
        }
        show_current();
        startTagActivity();   //tag activity starts upon model+current selection
    }
    private void show_current()
    {
        this.currentField.setText(Integer.toString(set_current)+"mA"); //update in static field
        this.inputCurrent.setText(Integer.toString(set_current)); //update in dynamic input field
    }
    private int filter_current(int crnt)
    {
        ProceedECG.setVisibility(View.VISIBLE);
        switch (driver_in_action)
        {
            case "B1x25W": case "B1x50W":
                if ((crnt < 120) || (crnt > 350))
                 {
                  Toast.makeText(getApplication(), R.string.bad_current, Toast.LENGTH_LONG).show();
                  return 350;
                 } else
                 {
                  ProceedECG.setText(R.string.proceed_ecg);
                  return crnt;
                 }
            case "B1x80W":
                if ((crnt < 120) || (crnt > 700))
                {
                    Toast.makeText(getApplication(), R.string.bad_current, Toast.LENGTH_LONG).show();
                    return 350;
                } else
                {
                    ProceedECG.setText(R.string.proceed_ecg);
                    return crnt;
                }
            case "RC80W": case "RC120W":
            if ((crnt < 700) || (crnt > 1050))
            {
                Toast.makeText(getApplication(), R.string.bad_current, Toast.LENGTH_LONG).show();
                return 700;
            } else
            {
                ProceedECG.setText(R.string.proceed_ecg);
                return crnt;
            }
            case "BI20W": case "BI40W":
            if ((crnt < 350) || (crnt > 700))
            {
                Toast.makeText(getApplication(), R.string.bad_current, Toast.LENGTH_LONG).show();
                return 350;
            } else
            {
                ProceedECG.setText(R.string.proceed_ecg);
                return crnt;
            }
            default:  //no model selected still/yet
                Toast.makeText(getApplication(), R.string.select_ecg, Toast.LENGTH_LONG).show();
                return 0;
        }
    }

//state machine for password management: messages from threads return statuses of each operation
    Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {//-----------------Virgin Tag being happily married------------------------------------
                case  DEFAULT_CFG_PWD_OK:  //factory config password confirmed - update PWM r/w permissions and change CFG pwd right after
                    Toast.makeText(getApplication(), getResources().getString(R.string.default_cfg_pwd_OK), Toast.LENGTH_LONG).show();
                    updatePWMsettings();   //done once only if default password being set
                    break;
                case B1_CFG_PWD_SET_OK:  //success in setting CFG pwd. Time to setup PWM pwd
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_cfg_pwd_upd), Toast.LENGTH_LONG).show();
                    check0pwdPWM();     //present default PWM pwd in order to proceed: if correct, setup PWM and set custom PWM password
                    break;
                case DEFAULT_PWM_PWD_OK:  //factory pwm password confirmed - it must be updated ASAP
                    Toast.makeText(getApplication(), getResources().getString(R.string.default_pwm_pwd_OK), Toast.LENGTH_LONG).show();
                    setB1pwdPWM();       //no need to enter old PWM pwd again
                    break;
                case PWM_SETUP_DONE:      //set CFG password upon PWM configuration - seems no need to present CFG pass again
                    Toast.makeText(getApplication(), getResources().getString(R.string.pwm_configured_OK), Toast.LENGTH_LONG).show();
                    setB1pwdCFG();         //set custom CFG password. PWM settings stay intact in all further PWD sessions with this tag
                    break;
                case B1_PWM_PWD_SET_OK:   //initial tag configuration completed. Allow further operations
                    pwd_NOT_OK = false;
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_all_pwd_set), Toast.LENGTH_LONG).show();
                    startTagActivity();  //custom PWM password assumes that PWM setting already made, so B1 ECG can be configured
                    break;
               //-----------------Virgin Еag being happily married (passwords set successfully)-------
               //_____________________________________________________________________________________
               //-----------------Tag is already protected by B1 passwords----------------------------
                case DEFAULT_CFG_PWD_NOT_OK: //tag already protected - find out if it has B1 pass or is owned by someone else
                    Toast.makeText(getApplication(), getResources().getString(R.string.default_cfg_pwd_NOT_OK), Toast.LENGTH_LONG).show();
                    checkB1pwdCFG();
                    break;
                case B1_CFG_PWD_OK: //tag protected with our CFG password - check PWM protection
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_cfg_pwd_pass), Toast.LENGTH_LONG).show();
                    check0pwdPWM();  //check if custom pwm pass not yet set
                    break;
                case B1_PWM_PWD_OK: //tag protected with our PWM password - enable B1 further configuration
                    pwd_NOT_OK = false;
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_pwm_pwd_pass), Toast.LENGTH_LONG).show();
                    startTagActivity();  //custom PWM password assumes that PWM setting already made, so B1 ECG can be configured
                    break;
               //-------Tag is already protected by B1 passwords (passwords presented successfully)---
               //_____________________________________________________________________________________
               //-----------------When something goes it`s own way------------------------------------
                case B1_CFG_PWD_SET_FAIL: //fail to set CFG pwd - just ask user to try one more time
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_cfg_pwd_upd_fail), Toast.LENGTH_LONG).show();
                    break;
                case B1_PWM_PWD_SET_FAIL: //fail to set PWM pwd - just ask user to try one more time
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_pwm_pwd_upd_fail), Toast.LENGTH_LONG).show();
                    break;
                case PWM_SETUP_FAIL:  //error during PWM settings update - try one more time
                    Toast.makeText(getApplication(), getResources().getString(R.string.pwm_configured_fail), Toast.LENGTH_LONG).show();
                    break;
                case DEFAULT_PWM_PWD_NOT_OK: //tag already protected - find out if it has B1 pass or is owned by someone else
                    Toast.makeText(getApplication(), getResources().getString(R.string.default_pwm_pwd_NOT_OK), Toast.LENGTH_LONG).show();
                    checkB1pwdPWM();   // proceed in case if B1 PWM pass is valid, disable further operations otherwise
                    break;
                case B1_PWM_PWD_NOT_OK: //tag protected with unknown PWM password - prevent further B1 configuration
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_pwm_pwd_reject), Toast.LENGTH_LONG).show();
                    pwd_NOT_OK = true;
                    break;
                case B1_CFG_PWD_NOT_OK: //tag protected with unknown CFG password - prevent further B1 configuration
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_cfg_pwd_reject), Toast.LENGTH_LONG).show();
                    pwd_NOT_OK = true;
                    break;
               //-----------------When something goes it`s own way (try again or drop it)------------
               //____________________________________________________________________________________
               //-----------------Tag asks for passwords during (re)configuration
                case PRESENT_CFG_PWD:  //change current CFG pwd or update PWM r/w rights require presenting CFG pwd
                    Toast.makeText(getApplication(), getResources().getString(R.string.cfg_pwd_asked), Toast.LENGTH_LONG).show();
                    break;  //just for log/debug, all passwords provided in advance
                case PRESENT_PWM_PWD:  //change current PWM pwd require presenting PWM pwd
                    Toast.makeText(getApplication(), getResources().getString(R.string.pwm_pwd_asked), Toast.LENGTH_LONG).show();
                    break;  //just for log/debug, all passwords provided in advance
                case PRESENT_CFG_PWD_FAILED:
                    break;
                case PRESENT_PWM_PWD_FAILED:
                    break;
               //-----------------General issues-----------------------------------------------------
                case NDA_FAIL:
                    Toast.makeText(getApplication(), getResources().getString(R.string.nda_protected), Toast.LENGTH_LONG).show();
                    break;
                case NOT_IN_FIELD:
                    Toast.makeText(getApplication(), getResources().getString(R.string.tag_not_in_the_field), Toast.LENGTH_LONG).show();
                    break;
                case COMMAND_FAILED:
                    Toast.makeText(getApplication(), getResources().getString(R.string.cfg_err), Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    };


    public void Open_link(View view)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.ltcompany.com/ru/"));
        startActivity(browserIntent);
    }

    //things to be cleaned up
    private void Run_B1_NFC_tool()
    {
        try {// Put here the debug features that you want to enable
            TagCache.class.getField("DBG_CACHE_MANAGER").set(null, false);
            NDEFRecord.class.getField("DBG_NDEF_RECORD").set(null, true);
            }
        catch (IllegalAccessException e)
            {
            e.printStackTrace();
            }
        catch (NoSuchFieldException e)
           {
            e.printStackTrace();
           }
    }

    //test button pressed
    public void run_test(View view)
    {
        ProceedECG.setVisibility(View.VISIBLE);
        if (driver_in_action.equals("Model")) //no driver selected yet
        {
            ProceedECG.setText(R.string.select_ecg);
            return;
        } else {
            Intent intent = new Intent(this, B1configActivity.class);
            intent.putExtra("ecg_name", driver_in_action);
            intent.putExtra("ecg_crnt", set_current);
            startActivity(intent);
        }
    }

}//main activity class