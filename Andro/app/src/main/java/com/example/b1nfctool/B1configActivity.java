package com.example.b1nfctool;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Handler;
import android.os.Message;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.st.st25sdk.STException;
import com.st.st25sdk.type5.STType5PasswordInterface;
import com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag;

import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.PWM1;
import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.PWM2;
import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.ST25DVPWM_PWM_MAX_FREQ;
import static com.st.st25sdk.type5.st25dvpwm.ST25DVPwmTag.ST25DVPWM_PWM_MIN_FREQ;


public class B1configActivity extends AppCompatActivity
{
    private android.nfc.NfcAdapter NfcAdapter; //android stuff
    private android.app.PendingIntent PendingIntent;

    private TextView ECG_name;
    private TextView ECG_current;

    public ST25DVPwmTag myTag = null;
    private STType5PasswordInterface mSTType5PasswordInterface;
    boolean AreTagPwmSettingsRead = false;
    //obtain data from threads
    private final int CURRENT_SET_OK = 0;    // успех
    private final int CURRENT_SET_FAIL = 1;  // общая ошибка установки тока
    private final int PWD_REQUIRED = 2;      // метка защишена паролем
    private final int NOT_IN_FIELD = 3;      // метка вне области ридера
    private final int DUTY_READ = 4;         // прочтен период ШИМ
    private final int FREQ_READ = 5;         // прочтена частота ШИМ
    private final int CURRENT_READ_FAIL = 6; // общая ошибка считывания тока
    private final int B1_PWM_PWD_OK = 7;     // пароль принят
    private final int B1_PWM_PWD_NOT_OK = 8; // пароль не принят
    //////////////// PWM1 /////////////defaults
    private boolean Pwm1Enable = true;
    private int Pwm1Frequency = 2000;
    private int Pwm1FrequencyOld = 0;
    private int Pwm1DutyCycle = 100; //value to be set to driver
    private int Pwm1DutyCycleOld = 0; //value being read from driver (for info)
    private TextView Duty1TextView;
    private TextView Duty1oldTextView;
    //////////////// PWM2 /////////////defaults
    private boolean Pwm2Enable = false;
    private int Pwm2Frequency = 2000;
    private int Pwm2DutyCycle = 100;

    private int ecg_current_mA;
    private byte[] pwm_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b1config);
        //get tag to work with
        myTag = (ST25DVPwmTag) MainActivity.getTag();
        mSTType5PasswordInterface = (STType5PasswordInterface) MainActivity.getTag();
        //prepare NFC stuff
        NfcAdapter = NfcAdapter.getDefaultAdapter(this);
        PendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        //display model name and current to be set
        ECG_name = (TextView)findViewById(R.id.ecg_name);
        ECG_current = (TextView)findViewById(R.id.ecg_current);
        //display tag PWM data
        Duty1TextView = (TextView) findViewById(R.id.duty1);
        Duty1oldTextView = (TextView) findViewById(R.id.duty1old);
        Bundle arguments = getIntent().getExtras();
        if(arguments!=null)  //retrieve parameters to work with
        {
            String  mData = arguments.get("ecg_crnt").toString();
            ECG_current.setText(mData+"mA");
            mData = arguments.get("ecg_name").toString();
            ECG_name.setText(mData);
            ecg_current_mA = arguments.getInt("ecg_crnt");
            pwm_pass = arguments.getByteArray("pwm_pwd");// password already presented in MainActivity, so it is still valid until tag removed
            //convert mA to PWM period considering particular model features
            switch (mData)
            {
                case "B1x25W": case "B1x50W":
                    Pwm1DutyCycle = (int) ( ((double)ecg_current_mA/(double)350)*100 );
                    break;
                case "B1x80W": case "BI20W": case "BI40W":
                    Pwm1DutyCycle = (int) ( ((double)ecg_current_mA/(double)700)*100 );
                    break;
                case "RC80W": case "RC120W":
                    Pwm1DutyCycle = (int) ( ((double)ecg_current_mA/(double)1050)*100 );
                    break;
                default:
                    Pwm1DutyCycle = 100;
            }
            Duty1TextView.setText(Integer.toString(Pwm1DutyCycle));
        }
    }

    //to catch up NFC event
    @SuppressLint("MissingSuperCall")
    @Override
    public void onNewIntent(Intent intent)
    {
        // onResume gets called after this to handle the intent
        setIntent(intent);
    }
    @Override
    public void onResume() //take on NFC not to allow system to popup it to other apps when tag detected
    {// Read Tag PWM settings if not yet done
        Intent intent = getIntent();
        super.onResume();
        if (NfcAdapter != null)
        {
            //  Log.v(TAG, "enableForegroundDispatch");
            NfcAdapter.enableForegroundDispatch(this, PendingIntent, null /*nfcFiltersArray*/, null /*nfcTechLists*/);
        }
        processIntent(intent);
    }
    //read and display PWM data of (re)attached tag
    void processIntent(Intent intent)
    {
        if(intent == null) {return; }
        new Thread(new Runnable()
        {
            Message msg;
            @Override
            public void run()
            {
                try {
                    if (myTag == null) {return; }
                    // Retrieve all the info from the tag for PWM1
                    final String mTagName = myTag.getName();
                    final String mTagDescription = myTag.getDescription();
                    final String mTagType = myTag.getTypeDescription();
                    boolean enable = myTag.isPwmEnable(PWM1);
                    int frequency = myTag.getPwmFrequency(PWM1);
                    int dutyCycle = myTag.getPwmDutyCycle(PWM1);
                    // PWMs settings read successfully
                    msg = handler.obtainMessage(DUTY_READ, dutyCycle, frequency);
                    handler.sendMessage(msg);
                    }
                catch (STException e)
                {
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        case ISO15693_BLOCK_PROTECTED: // PWM Control Block is password protected
                            handler.sendEmptyMessage(PWD_REQUIRED);
                            break;
                        default:
                            handler.sendEmptyMessage(CURRENT_READ_FAIL);
                            break;
                    }
                }
            }
        }).start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }


    @Override //release NFC for other apps and activities for system to offer
    public void onPause()
    {
        if (NfcAdapter != null)
        {
            try
            {
                NfcAdapter.disableForegroundDispatch(this);
                //  Log.v(TAG, "disableForegroundDispatch");
            }
            catch (IllegalStateException e)
            {
                //   Log.w(TAG, "Illegal State Exception disabling NFC. Assuming application is terminating.");
            }
            catch (UnsupportedOperationException e)
            {
                //  Log.w(TAG, "FEATURE_NFC is unavailable.");
            }
        }
        super.onPause();
    }

//update tag`s PWMs value on button press
    public void tag_update_pwm1(View view)
    {
        if (!AreTagPwmSettingsRead) // Ignore user input until the Tag's PWM settings have been read
        {
            //dbg
           // return;
        }
            showPwm1PeriodAndPulseWidth();
            updateTag(PWM1);
    }
    public void tag_calib_up_pwm1(View view)
    {
       if (Pwm1DutyCycle++ >=100)
       {
           Pwm1DutyCycle = 100;
       }
        showPwm1PeriodAndPulseWidth();
        updateTag(PWM1);
    }
    public void tag_calib_down_pwm1(View view)
    {
        if (Pwm1DutyCycle-- <= 5)
        {
            Pwm1DutyCycle = 5;
        }
        showPwm1PeriodAndPulseWidth();
        updateTag(PWM1);
    }

    public void About(View view)
    {
       // Toast.makeText(getApplication(), getResources().getString(R.string.about), Toast.LENGTH_LONG).show();
        Toast.makeText(getApplication(),
                "Версия 1.0.0"  + "\n" +
                      "Световые Технологии 2021" + "\n" +
                      "Powered by Al.Bk", Toast.LENGTH_LONG).show();
    }

    private void showPwm1PeriodAndPulseWidth()
    {
                    if (isPwmFrequencyOk(Pwm1Frequency) && isPwmDutyCycleOk(Pwm1DutyCycle))
                    {
                        int period = (int) (1000000000 / (Pwm1Frequency * ST25DVPwmTag.ST25DVPWM_PWM_RESOLUTION_NS));
                        Duty1TextView.setText(Integer.toString(Pwm1DutyCycle));
                        Duty1oldTextView.setText(Integer.toString(Pwm1DutyCycleOld));
                    } else
                        {
                            Duty1TextView.setText(R.string.general_na);
                        }
    }

    private boolean isPwmFrequencyOk(int frequency)
    {
        if ((frequency < ST25DVPWM_PWM_MIN_FREQ) || (frequency > ST25DVPWM_PWM_MAX_FREQ)) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isPwmDutyCycleOk(int dutyCycle)
    {
        if ((dutyCycle < 0) || (dutyCycle > 100)) {
            return false;
        } else {
            return true;
        }
    }
//update tag`s PWM data
    public void updateTag(final int pwmNumber)
    {
        if (myTag == null)
        {
            return;
        }
        //seems ST sdk works only in thread mode; simple calling tag functions throws exeption immediately..
       new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (pwmNumber == PWM1)
                      {
                        myTag.setPwmConfiguration(PWM1, Pwm1Frequency, Pwm1DutyCycle, Pwm1Enable);
                      }
                    else if (pwmNumber == PWM2)
                        {
                        myTag.setPwmConfiguration(PWM2, Pwm2Frequency, Pwm2DutyCycle, Pwm2Enable);
                        } else return;
                    handler.sendEmptyMessage(CURRENT_SET_OK);
                    }
                catch (STException e)
                {
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        case ISO15693_BLOCK_IS_LOCKED:// PWM Control Block is password protected
                            handler.sendEmptyMessage(PWD_REQUIRED);
                            break;
                        default:
                            handler.sendEmptyMessage(CURRENT_SET_FAIL);
                            break;
                    }
                    return;
                }
            }
        }).start();
    }

    Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case  CURRENT_SET_OK:
                    Toast.makeText(getApplication(), getResources().getString(R.string.pwm_controls_updated), Toast.LENGTH_LONG).show();
                    break;
                case CURRENT_SET_FAIL:
                    Toast.makeText(getApplication(), getResources().getString(R.string.failed_to_update_pwm_controls), Toast.LENGTH_LONG).show();
                    break;
                case CURRENT_READ_FAIL:
                    Toast.makeText(getApplication(), getResources().getString(R.string.failed_to_read_pwm_controls), Toast.LENGTH_LONG).show();
                    break;
                case PWD_REQUIRED:
                    Toast.makeText(getApplication(), getResources().getString(R.string.enter_pwm_password), Toast.LENGTH_LONG).show();
                    break;
                case NOT_IN_FIELD:
                    Toast.makeText(getApplication(), getResources().getString(R.string.tag_not_in_the_field), Toast.LENGTH_LONG).show();
                    break;
                case DUTY_READ:
                    Pwm1DutyCycleOld = msg.arg1;
                    Pwm1FrequencyOld = msg.arg2;
                    showPwm1PeriodAndPulseWidth();
                    presentPwmPassword(); //present PWM password for newly attached tag right after reading it
                    break;
                case B1_PWM_PWD_OK:
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_pwm_pwd_pass), Toast.LENGTH_LONG).show();
                    break;
                case B1_PWM_PWD_NOT_OK: //tag protected with unknown PWM password - prevent further B1 configuration
                    Toast.makeText(getApplication(), getResources().getString(R.string.custom_pwm_pwd_reject), Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    };


    private void presentPwmPassword()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int passwordID = ST25DVPwmTag.ST25DVPWM_PWM_PASSWORD_ID;
                try {
                    mSTType5PasswordInterface.presentPassword(passwordID, pwm_pass);
                    handler.sendEmptyMessage(B1_PWM_PWD_OK);  // Custom password presented successfull
                }
                catch (STException e) {// Present password failed
                    switch (e.getError())
                    {
                        case TAG_NOT_IN_THE_FIELD:
                            handler.sendEmptyMessage(NOT_IN_FIELD);
                            break;
                        default:
                            handler.sendEmptyMessage(B1_PWM_PWD_NOT_OK);
                            break;
                    }
                }
            }
        }).start();
    }

}