/**
  ******************************************************************************
  * @file    ndef_demo.c
  * @author  MMY Application Team
  * @version $Revision$
  * @date    $Date$
  * @ingroup ST25R3916_Discovery_Demo
  * @brief   NDEF display helper methods
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty  
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
#include "stdint.h"
#include "stdlib.h"
#include "Menu_config.h"
#include "st25_discovery_st25r.h"

#include "lib_NDEF_URI.h"
#include "lib_NDEF_Vcard.h"
#include "lib_NDEF_Email.h"

/* Reuse pictures */
extern const char nmark_64x64[];
extern const char cUrl[];
extern const char cVcard[];
extern const char cEmail[];

/* Method to display a URI NDEF message */
static void displayNdefUri(sURI_Info *uri, uint32_t maxChar)
{
  char txt[500] = {0};
  strcat(txt,uri->protocol);
  Menu_DisplayPicture(126,70,cUrl);
  if(strlen(uri->URI_Message) < 60)
  {
    strcat(txt,uri->URI_Message);
  } else {
    memcpy(&txt[strlen(uri->protocol)],uri->URI_Message,60);
    strcat(txt,"...");
  }
  if(strlen(txt) <= maxChar)
    BSP_LCD_DisplayStringAt(0,140, (uint8_t*)txt,CENTER_MODE);
  else
    Menu_DisplayLongString(20, 140, txt, maxChar);          
}

/* Method to display a vCard NDEF message */
void displayNdefVCard(sVcardInfo *vcard,  sRecordInfo_t *record, uint32_t maxChar)
{
      uint16_t y_label = 86;
      if(strlen(vcard->FirstName))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->FirstName, LEFT_MODE);
        y_label += 16;
      } else if (strlen(vcard->Name))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->Name, LEFT_MODE);
        y_label += 16;
      }
      if(strlen(vcard->Org))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->Org, LEFT_MODE);
        y_label += 16;
      }
      if(strlen(vcard->WorkTel))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->WorkTel, LEFT_MODE);
        y_label += 16;
      } else if (strlen(vcard->HomeTel))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->HomeTel, LEFT_MODE);
        y_label += 16;
      } else if (strlen(vcard->CellTel))
      {
        BSP_LCD_DisplayStringAt(100, y_label, (uint8_t*)vcard->CellTel, LEFT_MODE);
        y_label += 16;
      }

      y_label = 140;
      if(strlen(vcard->WorkEmail))
      {
        BSP_LCD_DisplayStringAt(20, y_label, (uint8_t*)vcard->WorkEmail, LEFT_MODE);
        y_label += 16;
      } else if (strlen(vcard->Email))
      {
        BSP_LCD_DisplayStringAt(20, y_label, (uint8_t*)vcard->Email, LEFT_MODE);
        y_label += 16;
      }
      char* display_str= NULL;
      if(strlen(vcard->WorkAddress))
      {
        display_str = vcard->WorkAddress;
      } else if (strlen(vcard->Address))
      {
        display_str = vcard->Address;
      } else if (strlen(vcard->HomeAddress))
      {
        display_str = vcard->HomeAddress;
      }
      y_label += Menu_DisplayLongString(20, y_label, display_str, maxChar) * 16;
      BSP_LCD_DisplayPicture(20,70,cVcard);
}

/* Method to display an email NDEF message */
void displayNdefEmail(sEmailInfo *email)
{
  Menu_SetStyle(GREY);
  uint16_t y_label = 70;
  uint16_t x_label = 90;
  BSP_LCD_DisplayStringAt(x_label,y_label,(uint8_t*)"to:",LEFT_MODE);
  Menu_SetStyle(PLAIN);
  x_label = 90 + strlen("to:")*BSP_LCD_GetFont()->Width;
  y_label += Menu_DisplayLongString(x_label, y_label, email->EmailAdd, (300 - x_label) / BSP_LCD_GetFont()->Width) * 16;
  Menu_SetStyle(GREY);
  x_label = 90;
  BSP_LCD_DisplayStringAt(x_label,y_label, (uint8_t*)"sub:",LEFT_MODE);
  Menu_SetStyle(PLAIN);
  x_label = 90 + strlen("sub:")*BSP_LCD_GetFont()->Width;
  y_label += Menu_DisplayLongString(x_label, y_label, email->Subject, (300 - x_label) / BSP_LCD_GetFont()->Width) * 16;
  y_label = y_label > 140 ? y_label : 140;
  y_label += Menu_DisplayLongString(20, y_label, email->Message, 300 / BSP_LCD_GetFont()->Width) * 16;
  BSP_LCD_DisplayPicture(20,70,cEmail);
}

/* Method to display any other than previous NDEF message */
void displayNdefGeneric(sRecordInfo_t *record)
{
  // Other record
  char type[41] = {0};
  char txt[60] = {0};
  memcpy(type,record->Type, record->TypeLength > 40 ? 40 :  record->TypeLength);
  Menu_DisplayPicture(240,70,nmark_64x64);

  sprintf(txt,"Length: %d Bytes",record->PayloadLength);
  BSP_LCD_DisplayStringAt(20,86,(uint8_t*)txt,LEFT_MODE);
  sprintf(txt,"Flag:   0x%02X",record->RecordFlags);
  BSP_LCD_DisplayStringAt(20,102,(uint8_t*)txt,LEFT_MODE);
  uint32_t smallTypeLen = ((300 -64) / BSP_LCD_GetFont()->Width ) - 8;
  if(record->TypeLength  < smallTypeLen)
  {
    sprintf(txt,"Type:   %s",type);
    BSP_LCD_DisplayStringAt(20,118,(uint8_t*)txt,LEFT_MODE);
  } else {
    BSP_LCD_DisplayStringAt(20,118,(uint8_t*)"Type: ",LEFT_MODE);
    Menu_DisplayLongString(20, 134, type, 300 / BSP_LCD_GetFont()->Width);
  }

}

/**
  * @brief Helper method to display a NDEF message on demo screen.
  * @param ndefBuffer The buffer containing an ndef message.
  * @returns status 0 is success, other values are errors.
  */
uint8_t displayNdef(uint8_t *ndefBuffer)
{
  sRecordInfo_t record;
  union {
    sURI_Info uri;
    sVcardInfo vcard;
    sEmailInfo email;
  } buffer = {0};
  uint8_t status;

  Menu_SetStyle(PLAIN);
  BSP_LCD_SetFont(&Font16);

  status = NDEF_IdentifyBuffer(&record,ndefBuffer);
  if(status == NDEF_OK)
  {
    uint32_t maxChar = 270 / Menu_GetFontWidth();

    if(NDEF_ReadURI(&record,&buffer.uri) == NDEF_OK)
    {
      displayNdefUri(&buffer.uri,maxChar);
    } else if (NDEF_ReadVcard(&record,&buffer.vcard) == NDEF_OK)
    {
      displayNdefVCard(&buffer.vcard,  &record, maxChar);
    } else if (NDEF_ReadEmail(&record,&buffer.email) == NDEF_OK) {
      displayNdefEmail(&buffer.email);
    } else {
      displayNdefGeneric(&record);
    }
  }
  BSP_LCD_SetFont(&Font22);
  return status;
}
