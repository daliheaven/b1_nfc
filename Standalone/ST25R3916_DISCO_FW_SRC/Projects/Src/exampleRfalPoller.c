/******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
******************************************************************************/


/*
 ******************************************************************************
 * INCLUDES
 ******************************************************************************
 */
// Generic includes
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

// reader includes
#include "st_errno.h"
#include "platform.h"
#include "rfal_nfca.h"
#include "rfal_nfcb.h"
#include "rfal_nfcf.h"
#include "rfal_nfcv.h"
#include "rfal_st25tb.h"
#include "rfal_isoDep.h"
#include "rfal_nfcDep.h"
#include "rfal_AnalogConfig.h"
#include "utils.h"
#include "st25r3916.h"
#include "rfal_dpo.h"

// tag content
#include "lib_NDEF.h"
#include "lib_NDEF_URI.h"

#include "st25_discovery_st25r.h"
#include "TruST25.h"
#include "ndef_display.h"

/* Display management */
#include "st25_discovery_lcd.h"
#include "Menu_core.h"
#include "Menu_config.h"
#include "Menu_tab.h"
#include "cartouche_jpg.h"
#include "send_icon_jpg.h"
#include "Error_jpg.h"
#include "Success_jpg.h"
#include "math.h"


//#define FREEZE_DISPLAY 1
/*
******************************************************************************
* GLOBAL DEFINES
******************************************************************************
*/
#define EXAMPLE_RFAL_POLLER_DEVICES      4    /* Number of devices supported */

#define EXAMPLE_RFAL_POLLER_FOUND_NONE   0x00  /* No device found Flag        */
#define EXAMPLE_RFAL_POLLER_FOUND_A      0x01  /* NFC-A device found Flag     */
#define EXAMPLE_RFAL_POLLER_FOUND_B      0x02  /* NFC-B device found Flag     */
#define EXAMPLE_RFAL_POLLER_FOUND_F      0x04  /* NFC-F device found Flag     */
#define EXAMPLE_RFAL_POLLER_FOUND_V      0x08  /* NFC-V device Flag           */
#define EXAMPLE_RFAL_POLLER_FOUND_ST25TB 0x10  /* ST25TB device found flag */

#define RADIO_X 20 /* Radio button X position (to select tag detection method) */
#define RADIO_Y 50 /* Radio button Y position (to select tag detection method) */

/*
******************************************************************************
* GLOBAL TYPES
******************************************************************************
*/

/*! Main state                                                                            */
typedef enum{
    EXAMPLE_RFAL_POLLER_STATE_INIT                =  0,  /* Initialize state            */
    EXAMPLE_RFAL_POLLER_STATE_TECHDETECT          =  1,  /* Technology Detection state  */
    EXAMPLE_RFAL_POLLER_STATE_COLAVOIDANCE        =  2,  /* Collision Avoidance state   */
    EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION        =  9   /* Deactivation state          */
}exampleRfalPollerState;


/*! Device interface                                                                      */
typedef enum{
    EXAMPLE_RFAL_POLLER_INTERFACE_RF     = 0,            /* RF Frame interface          */
    EXAMPLE_RFAL_POLLER_INTERFACE_ISODEP = 1,            /* ISO-DEP interface           */
    EXAMPLE_RFAL_POLLER_INTERFACE_NFCDEP = 2             /* NFC-DEP interface           */
}exampleRfalPollerRfInterface;

/*! Device struct containing all its details                                              */
typedef struct{
    BSP_NFCTAG_Protocol_Id_t type;                      /* Device's type                */
    union{
        rfalNfcaListenDevice nfca;                      /* NFC-A Listen Device instance */
        rfalNfcbListenDevice nfcb;                      /* NFC-B Listen Device instance */
        rfalNfcfListenDevice nfcf;                      /* NFC-F Listen Device instance */
        rfalNfcvListenDevice nfcv;                      /* NFC-V Listen Device instance */
        rfalSt25tbListenDevice st25tb;                      /* NFC-V Listen Device instance */
    }dev;                                               /* Device's instance            */
    union{
        rfalIsoDepDevice isoDep;                        /* ISO-DEP instance             */
        rfalNfcDepDevice nfcDep;                        /* NFC-DEP instance             */
    }proto;                                             /* Device's protocol            */
    uint8_t NdefSupport;
    exampleRfalPollerRfInterface rfInterface;           /* Device's interface           */
}exampleRfalPollerDevice;

/** Detection mode for the demo */
typedef enum {
  DETECT_MODE_POLL = 0,     /** Continuous polling for tags */
  DETECT_MODE_WAKEUP = 1,   /** Waiting for the ST25R3916 wakeup detection */
  DETECT_MODE_AWAKEN = 2    /** Awaken by the ST25R3916 wakeup feature */
} detectMode_t;

/*
 ******************************************************************************
 * LOCAL VARIABLES
 ******************************************************************************
 */
static uint8_t                 gDevCnt;                                 /* Number of devices found                         */
static exampleRfalPollerDevice gDevList[EXAMPLE_RFAL_POLLER_DEVICES];   /* Device List                                     */
static exampleRfalPollerState  gState;                                  /* Main state                                      */
static uint8_t                 gTechsFound;                             /* Technologies found bitmask                      */
exampleRfalPollerDevice        *gActiveDev;                             /* Active device pointer                           */

static rfalBitRate rxbr,txbr;                                           /* Detected tag bitrate information */
static bool instructionsDisplayed = false;                              /* Keep track of demo instruction display */
static const uint16_t yBox = 24;                                        /* Detected tag box margin*/
static const uint16_t boxHeight = 45;                                   /* Detected tag box height */
static const uint16_t boxSpace = 4;                                     /* Detected tag box spacing */
static detectMode_t detectMode = DETECT_MODE_POLL;                      /* Current tag detection method */
static uint16_t lineY[] = {75, 100, 116, 141, 157, 182, 192};           /* Tag information line position */
static char type1Str[] =  "Type 1 Tag";                                 /* NFC Forum Tag Type1 string */
static char type2Str[] =  "Type 2 Tag";                                 /* NFC Forum Tag Type2 string */
static char type3Str[] =  "Type 3 Tag";                                 /* NFC Forum Tag Type3 string */
static char type4aStr[] = "Type 4A Tag";                                /* NFC Forum Tag Type4A string */
static char type4bStr[] = "Type 4B Tag";                                /* NFC Forum Tag Type4B string */
static char type5Str[] =  "Type 5 Tag";                                 /* NFC Forum Tag Type5string */
static char iso15693Str[] =  "Iso15693 Tag";                            /* iso15693 Tag string */
static char st25tbStr[] = "ST25TB Tag";                                 /* ST25TB tag string */
static char iso14443aStr[] = "Iso14443A Tag";                           /* Iso14443A tag string */
static char felicaStr[] = "Felica Tag";                                 /* Felica tag string */
static const char *dpoProfile[] = {"Full power", "Med. power", "Low power "}; /* Dynamic Power Output profile strings*/
static uint16_t lastRssi;                                               /* RSSI history value */
static uint16_t uidLine = 1;                                            /* Line Id to display Tag UID */
static uint16_t hbrLine = 3;                                            /* Line Id to display High Bitrate info */
static uint16_t rssiLine = 5;                                           /* Line Id to display RSSI info */
static uint16_t rssiBarLine = 6;                                        /* Line Id to display RSSI info as a bar */
static uint16_t xMargin = 15;                                           /* Tag information margin */
static uint32_t timeRssi = 0;                                           /* Use to detect long latency in tag response */

/*
******************************************************************************
* LOCAL FUNCTION PROTOTYPES
******************************************************************************
*/
static bool exampleRfalPollerTechDetection( void );
static bool exampleRfalPollerCollResolution( void );
static bool exampleRfalPollerDeactivate( void );
static void exampleRfalPollerRun( void );

/* Compute detected tag Y position */
static uint16_t getTagBoxY(int index)
{
  return yBox + index*(boxHeight + boxSpace);
}

/* Display a detected tag on the LCD screen (max: 4 tags detected) */
static void displayTag(int index, char* type, uint8_t *uid)
{
  const uint16_t xBox = 8;
  const uint16_t boxWidth = 304;
  const uint16_t boxCornerRadius = 10;
  const uint16_t yText = 15;

  char str[30] = ""; 
  char uid_str[30] = ""; 
  if (index > 3)
    return;

  if(uid != NULL)
  {
    strcat(str,type);
    for(int c = strlen(type); c < 7; c++)
      strcat(str," ");
    sprintf(uid_str,"%02X:%02X:%02X:%02X",uid[0],uid[1],uid[2],uid[3]);
    strcat(str,uid_str);
  } else {
    strcpy(str, "                  ");
  }

#ifndef FREEZE_DISPLAY
  BSP_LCD_DrawRectangleWithRoundCorner(xBox,getTagBoxY(index),boxWidth,boxHeight,boxCornerRadius);
  Menu_DisplayStringAt(Menu_GetFontWidth(),yText + Menu_GetFontHeight() + index * (boxHeight + boxSpace), str);
#endif
}

/* Control Radio Button display */
static void setRadio(void)
{
#ifndef FREEZE_DISPLAY
  if(detectMode == DETECT_MODE_POLL)
  {
    BSP_LCD_SetColors(LCD_COLOR_BLUE2,0xFFFF);
    BSP_LCD_FillCircle(RADIO_X,RADIO_Y, 5);
    BSP_LCD_SetColors(0xFFFF,0xFFFF);
    BSP_LCD_FillCircle(RADIO_X,RADIO_Y+40, 5);
  } else {
    BSP_LCD_SetColors(LCD_COLOR_BLUE2,0xFFFF);
    BSP_LCD_FillCircle(RADIO_X,RADIO_Y+40, 5);
    BSP_LCD_SetColors(0xFFFF,0xFFFF);
    BSP_LCD_FillCircle(RADIO_X,RADIO_Y, 5);
  }
#endif
}

/* Control display when no tag is detected (instruction + radio button) */
void tagDetectionNoTag(void)
{
#ifndef FREEZE_DISPLAY

  BSP_LCD_SetColors(0x0000,0xFFFF);

  BSP_LCD_FillCircle(RADIO_X,RADIO_Y, 10);
  BSP_LCD_SetColors(0xFFFF,0x0000);
  BSP_LCD_FillCircle(RADIO_X,RADIO_Y, 7);


  BSP_LCD_SetColors(0x0000,0xFFFF);
  BSP_LCD_FillCircle(RADIO_X,RADIO_Y+40, 10);
  BSP_LCD_SetColors(0xFFFF,0x0000);
  BSP_LCD_FillCircle(RADIO_X,RADIO_Y+40, 7);

  setRadio();

  Menu_SetStyle(PLAIN);
  BSP_LCD_DisplayStringAt(RADIO_X + 20,RADIO_Y-10,(uint8_t*)"Continuous Poll",LEFT_MODE);
  BSP_LCD_DisplayStringAt(RADIO_X + 20,RADIO_Y+30,(uint8_t*)"Wake-up",LEFT_MODE);
  Menu_DisplayCenterString(6,"Place tags above");
  Menu_DisplayCenterString(7,"the antenna...");
  instructionsDisplayed = true;
#endif
}

/* When a tag is selected, clear tab before displaying information */
static void clearTab(void)
{
  Menu_SetStyle(CLEAR_PLAIN);
  Menu_FillRectangle(12,64,296, 139);
}


/* Helper method to retrieve UID from a tag structure */
static bool getDevUid(uint8_t** uid, uint8_t* uidLen)
{
    switch (gActiveDev->type)
    {
      case BSP_NFCTAG_NFCA:
        *uid = gActiveDev->dev.nfca.nfcId1;
        *uidLen = gActiveDev->dev.nfca.nfcId1Len;
      break;
      case BSP_NFCTAG_NFCB:
        *uid = gActiveDev->dev.nfcb.sensbRes.nfcid0;
        *uidLen = RFAL_NFCB_NFCID0_LEN;
      break;
      case BSP_NFCTAG_ST25TB:
        *uid = gActiveDev->dev.st25tb.UID;
        *uidLen = RFAL_ST25TB_UID_LEN;
      break;
      case BSP_NFCTAG_NFCF:
        *uid = gActiveDev->dev.nfcf.sensfRes.NFCID2;
        *uidLen = RFAL_NFCF_NFCID2_LEN;
      break;
      case BSP_NFCTAG_NFCV:
        *uid = gActiveDev->dev.nfcv.InvRes.UID;
        *uidLen = RFAL_NFCV_UID_LEN;
      break;
      default:
        return false;
    }
  return true;
}

/* Helper method to define tag information position */
uint16_t yLine(uint8_t l)
{
  return lineY[l];
}

/* Helper method to get the tag type as a string */
static char* getTypeStr(void)
{
  char* retPtr = NULL;
  // only for Felica check
  uint8_t buffer[16];
  switch(gActiveDev->type)
  {
    case BSP_NFCTAG_NFCA:
      switch (gActiveDev->dev.nfca.type)
      {
        case RFAL_NFCA_T1T:
          retPtr = type1Str;
        break;
        case RFAL_NFCA_T2T:
          retPtr = type2Str;
        break;
        case RFAL_NFCA_T4T:
          if(gActiveDev->NdefSupport)
            retPtr = type4aStr;
          else
            retPtr = iso14443aStr;
        break;
      }
    break;
    case BSP_NFCTAG_NFCB:
      retPtr = type4bStr;
    break;
    case BSP_NFCTAG_ST25TB:
      retPtr = st25tbStr;
    break;
    case BSP_NFCTAG_NFCF:
      if(BSP_NFCTAG_ReadData(buffer,0,1) == ERR_NONE)
        retPtr = type3Str;
      else
        retPtr = felicaStr;
    break;
    case BSP_NFCTAG_NFCV:
      if((!gActiveDev->NdefSupport) || (BSP_NFCTAG_CheckVicinity()))
      {
        retPtr = iso15693Str;
      } else {
        retPtr = type5Str;
      }
    break;
  }
  return retPtr;
}

/* Helper method to get the current tag bitrate as numerical b/s */
static uint16_t getSpeedVal(rfalBitRate br)
{
  if(br <= RFAL_BR_848)
    return 106 * (int)pow(2,br);
  else if (br == RFAL_BR_52p97 )
    return 52;
  else if (br == RFAL_BR_26p48)
    return 26;
  else
    return 00;
}

/* Helper method to get the current tag bitrate as a string */
static void getSpeedStr(char *retPtr)
{
  rfalGetBitRate(&txbr,&rxbr);

  sprintf(retPtr, "%d-%d kb/s",
          getSpeedVal(rxbr),
          getSpeedVal(txbr));
}

/* When a tag is slected, dispays the current DPO profile */
static void displayDpo(rfalDpoEntry *currentDpo)
{
  const char *txt;
  uint16_t rfoX = 170;
  BSP_LCD_SetFont(&Font16);
  uint16_t rfoWidth = 10*BSP_LCD_GetFont()->Width +20;
  uint16_t rfoTxtX;

  if(currentDpo != NULL)
  {
    BSP_LCD_SetColors(BSP_LCD_FadeColor(LCD_COLOR_BLUEST,LCD_COLOR_LIGHTBLUE,4,currentDpo->rfoRes),LCD_COLOR_WHITE);
    txt = dpoProfile[currentDpo->rfoRes];
    rfoTxtX = rfoX + (rfoWidth - strlen(txt)*BSP_LCD_GetFont()->Width) / 2;

    BSP_LCD_DrawRectangleWithRoundCorner(rfoX,
                                         yLine(0)-4 - 5,
                                         rfoWidth,
                                         BSP_LCD_GetFont()->Height+14,
                                         10);
    BSP_LCD_DisplayStringAt(rfoTxtX,yLine(0),(uint8_t*)txt,LEFT_MODE);

    BSP_LCD_SetFont(&Font22);
    Menu_SetStyle(PLAIN);
  }
}

/* Callback function used to detect long latenncy in tag response, displays "Wait..." and avoid display freeze */
static uint8_t waitRssi(void)
{
  static uint8_t checkTouch = 0;
  Menu_Position_t touch;

  uint16_t rssiX = xMargin + 5 * BSP_LCD_GetFont()->Width;
    uint32_t delay = ((int32_t) HAL_GetTick() - (int32_t)timeRssi);
    if(delay > 1000)
    {
      BSP_LCD_DisplayStringAt(rssiX,yLine(rssiLine),(uint8_t*)"Wait...  ",LEFT_MODE);
      lastRssi = 0;
    }
    if(checkTouch++ > 50)
    {
      checkTouch = 0;
      Menu_ReadPosition(&touch);
      if((touch.Sel) && (touch.Y > 60) )
        // exit
        return 0;
    }
  // continue
  return 1;
}

/* Tag information Tab initial callback function */
static Menu_Callback_Status_t tabFirstTagReadInfo(void)
{
  char line[40] = "";

  clearTab();
  uint8_t nbUidDigit = 8;
  uint8_t *devUid;

  char speedStr[20];
  // re-init lastRssi at dummy value != 0
  lastRssi = 1;
  BSP_NFCTAG_WaitingCb = &waitRssi;

  getDevUid(&devUid,&nbUidDigit);
  for(int d = 0; d < nbUidDigit; d++)
  {
    if(gActiveDev-> type != BSP_NFCTAG_NFCV)
      sprintf(line,"%s%02X:",line,devUid[d]);
    else
      // for type V invert UID
      sprintf(line,"%s%02X:",line,devUid[nbUidDigit - d - 1]);
  }
  // remove last colon 
  line[strlen(line)-1] = '\0';

  Menu_SetStyle(PLAIN);
  BSP_LCD_SetFont(&Font16);

  BSP_LCD_DisplayStringAt(xMargin,yLine(0), (uint8_t*)getTypeStr(),LEFT_MODE);

  BSP_LCD_DisplayStringAt(xMargin,yLine(uidLine),(uint8_t*)"UID:",LEFT_MODE);
  BSP_LCD_DisplayStringAt(xMargin + 1 * Menu_GetFontWidth(),yLine(uidLine+1),(uint8_t*)line,LEFT_MODE);


  BSP_LCD_DisplayStringAt(xMargin,yLine(hbrLine),(uint8_t*)"Speed:",LEFT_MODE);
  getSpeedStr(speedStr);
  BSP_LCD_DisplayStringAt(xMargin + 1 * Menu_GetFontWidth(),yLine(hbrLine+1),(uint8_t*)speedStr,LEFT_MODE);


  BSP_LCD_DisplayStringAt(xMargin,yLine(rssiLine),(uint8_t*)"RSSI:",LEFT_MODE);

#ifdef ENABLE_TRUST25_SIGNATURE_VERIFICATION
  uint16_t trust25Line = 3;
  TruST25_Status_t TruST25_valid = TruST25_SignatureVerification(gActiveDev->type,gActiveDev->dev.nfca.type,devUid,nbUidDigit);
  if(TruST25_valid == TRUST25_VALID)
  {
    BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_DARKGREEN);
    BSP_LCD_DisplayPicture(170,yLine(trust25Line),Success);
    BSP_LCD_DisplayStringAt(205,yLine(trust25Line),(uint8_t*)"TruST25",LEFT_MODE);   
    BSP_LCD_DisplayStringAt(205,yLine(trust25Line)+16,(uint8_t*)"Validated",LEFT_MODE);   
    Menu_SetStyle(PLAIN);
  } else if (TruST25_valid == TRUST25_INVALID) {
    BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_RED);
    BSP_LCD_DisplayPicture(170+10,yLine(trust25Line),Error);
    BSP_LCD_DisplayStringAt(205+10,yLine(trust25Line),(uint8_t*)"TruST25",LEFT_MODE);   
    BSP_LCD_DisplayStringAt(205+10,yLine(trust25Line)+16,(uint8_t*)"Error!",LEFT_MODE);   
    Menu_SetStyle(PLAIN);
  }
#endif

  BSP_LCD_SetFont(&Font22);

  return MENU_CALLBACK_CONTINUE;
}

/* Tag Information Tab callback: displays the RSSI & DPO profile */
static Menu_Callback_Status_t tabTagReadInfo(void)
{
  uint8_t buffer[4];
  uint16_t rssi = 0;
  uint16_t rssiLen = 0;
  uint16_t front,back;
  static uint16_t execCount = 0;
  uint8_t status;


  if(execCount++ > 100)
  {
    execCount = 0;
    return MENU_CALLBACK_CONTINUE;
  }
  BSP_LCD_SetFont(&Font16);
  uint16_t rssiX = xMargin + BSP_LCD_GetFont()->Width;
  uint16_t rssi_max = 280 - 2*BSP_LCD_GetFont()->Width;


  timeRssi = HAL_GetTick();
  status = BSP_NFCTAG_ReadData(buffer,0,1);
  rfalGetTransceiveRSSI( &rssi );
  // with short frames, at High Bit Rate, rssi computation may not be relevant
  if((status == NFCTAG_OK) && (rssi == 0))
  {
    status = BSP_NFCTAG_ReadData(buffer,0,8);
    rfalGetTransceiveRSSI( &rssi );
  }
  if((status != NFCTAG_OK) && (status != NFCTAG_RESPONSE_ERROR))
  {
    // try to re-activate
    rfalFieldOff();
    HAL_Delay(5);
    timeRssi = HAL_GetTick();
    BSP_NFCTAG_Activate((BSP_NFCTAG_Device_t *)gActiveDev);
    rfalGetTransceiveRSSI( &rssi );
  }

  rssiLen = ceil ( (float)(rssi_max * rssi) / (float)0x550);
  rssiLen = rssiLen > rssi_max ? rssi_max : rssiLen;

  // don't update rssi display if rssi has not been measured
  if ((status == NFCTAG_OK) && (rssi == 0)) {
   rssiLen = lastRssi;
  }


  Menu_SetStyle(PLAIN);
  if((rssiLen == 0) && (lastRssi != 0))
  {
    BSP_LCD_GetColors(&front,&back);
    BSP_LCD_SetColors(back,front);
    BSP_LCD_FillRect(rssiX + 6,yLine(rssiBarLine) + 6,rssi_max,2);
    BSP_LCD_SetColors(front,back);
    BSP_LCD_DisplayStringAt(rssiX+ 4 * BSP_LCD_GetFont()->Width,yLine(rssiLine),(uint8_t*)"Link lost",LEFT_MODE);
  } else if (rssiLen != 0) {
    if(lastRssi == 0)
      BSP_LCD_DisplayStringAt(rssiX+ 4 * BSP_LCD_GetFont()->Width,yLine(rssiLine),(uint8_t*)"         ",LEFT_MODE);
    char rssiTxt[50];
    sprintf(rssiTxt,"       ");
    BSP_LCD_DisplayStringAt(rssiX+ 4 * BSP_LCD_GetFont()->Width,yLine(rssiLine),(uint8_t*)rssiTxt,LEFT_MODE);

    BSP_LCD_GetColors(&front,&back);
    BSP_LCD_SetColors(back,front);
    BSP_LCD_FillRect(BSP_LCD_GetXSize()/2 - rssi_max/2,yLine(rssiBarLine) + 6,rssi_max/2 - lastRssi/2,2);
    BSP_LCD_FillRect(BSP_LCD_GetXSize()/2 + lastRssi/2 + 6,yLine(rssiBarLine) + 6,rssi_max/2 - lastRssi/2,2);
    BSP_LCD_SetColors(front,back);
    BSP_LCD_FillRect(BSP_LCD_GetXSize()/2 - rssiLen/2,yLine(rssiBarLine) + 6, rssiLen,2);
  }

  lastRssi = rssiLen;

  /* DPO */
  displayDpo(rfalDpoGetCurrentTableEntry());
 
  BSP_LCD_SetFont(&Font22);

  return MENU_CALLBACK_CONTINUE;
}

/* Tag content Tab initial callback: display NDEF tag content*/
static Menu_Callback_Status_t tabFirstTagReadNdef(void)
{
  char txt[50] = {0};

  uint8_t status;
  uint16_t ndefLen = 0;
  uint8_t* ndefBuffer = NULL;
  clearTab();

  Menu_SetStyle(PLAIN);
  BSP_LCD_SetFont(&Font16);

  // disable NFCTAG loop callback
  BSP_NFCTAG_WaitingCb = NULL;

  // reset start of NDEF buffer
  NDEF_getNDEFSize(&ndefLen);
  if(ndefLen > sizeof(NDEF_Buffer))
  {
    ndefBuffer = (uint8_t *)malloc(ndefLen+2);
    if(ndefBuffer == NULL)
    {
      sprintf(txt, "NDEF is too big!");
      BSP_LCD_DisplayStringAt(0,102,(uint8_t*)txt,CENTER_MODE);
      status = NDEF_ERROR;
    }
  } else {
    ndefBuffer = NDEF_Buffer;
  }
  memset(ndefBuffer,0,100);
  status = NDEF_ReadNDEF(ndefBuffer);
  if((status == NDEF_OK) && ndefLen)
  {
    if(displayNdef(ndefBuffer) != NDEF_OK)
    {
      sprintf(txt, "Cannot identify NDEF record");
      Menu_SetStyle(PLAIN);
      BSP_LCD_SetFont(&Font16);
      BSP_LCD_DisplayStringAt(0,70,(uint8_t*)txt,CENTER_MODE);
      char data_str[30] = {0};
      status = BSP_NFCTAG_ReadData(ndefBuffer,0,16);
      if(status == NFCTAG_OK)
      {
        for(int data_index = 0; data_index < 4 ; data_index++)
        {
          sprintf(data_str,"0x%02X, 0x%02X, 0x%02X, 0x%02X",ndefBuffer[data_index*4],
                                                            ndefBuffer[data_index*4+1],
                                                            ndefBuffer[data_index*4+2],
                                                            ndefBuffer[data_index*4+3]);
          BSP_LCD_DisplayStringAt(0,86 + (data_index * 16),(uint8_t*)data_str,CENTER_MODE);
        }
      } else {
        sprintf(txt, "Cannot read data");
        BSP_LCD_DisplayStringAt(0,102,(uint8_t*)txt,CENTER_MODE);
      }
    }

  } else {
    if(status == NDEF_ERROR_MEMORY_INTERNAL)
    {
      sprintf(txt, "NDEF is too big");      
    } else {
      sprintf(txt, "Cannot read NDEF");
    }

    char data_str[30] = {0};
    status = BSP_NFCTAG_ReadData(NDEF_Buffer,0,16);

    Menu_SetStyle(PLAIN);
    BSP_LCD_SetFont(&Font16);
    BSP_LCD_DisplayStringAt(0,70,(uint8_t*)txt,CENTER_MODE);

    if(status == NFCTAG_OK)
    {
      for(int data_index = 0; data_index < 4 ; data_index++)
      {
        sprintf(data_str,"0x%02X, 0x%02X, 0x%02X, 0x%02X",NDEF_Buffer[data_index*4],
                                                          NDEF_Buffer[data_index*4+1],
                                                          NDEF_Buffer[data_index*4+2],
                                                          NDEF_Buffer[data_index*4+3]);
        BSP_LCD_DisplayStringAt(0,86 + (data_index * 16),(uint8_t*)data_str,CENTER_MODE);
      }
    } else {
      sprintf(txt, "Cannot read data");
      BSP_LCD_DisplayStringAt(0,102,(uint8_t*)txt,CENTER_MODE);
    }

    BSP_LCD_SetFont(&Font22);
  }

  // write ndef icon
  if(gActiveDev->NdefSupport)
    BSP_LCD_DisplayPicture(260,160, send_icon);

  if((ndefBuffer != NDEF_Buffer) && (ndefBuffer != NULL))
  {
    free(ndefBuffer);
    ndefBuffer = NULL;
  }
  return MENU_CALLBACK_CONTINUE;

}

/* Tag content Tab callback: manage NDEF message write */
static Menu_Callback_Status_t tabTagReadNdef(void)
{
  Menu_Position_t touch;

  if(!gActiveDev->NdefSupport)
    return MENU_CALLBACK_CONTINUE;

  if(Menu_ReadPosition(&touch))
  {
    if((touch.X > 250) && (touch.Y > 150))
    {
      uint16_t length = 0;
      sURI_Info w_uri = {URI_ID_0x01_STRING, "st.com/st25r3916-demo" ,""};
      NDEF_PrepareURIMessage(&w_uri,NDEF_Buffer,&length);
      if(NfcTag_WriteNDEF(length,NDEF_Buffer) == NFCTAG_OK)
      {
        clearTab();
        Menu_SetStyle(PLAIN);
        BSP_LCD_SetFont(&Font16);
        BSP_LCD_DisplayStringAt(0,120, (uint8_t*)"NDEF write success!", CENTER_MODE);
        HAL_Delay(500);
        tabFirstTagReadNdef();
      } else {
        clearTab();
        Menu_SetStyle(PLAIN);
        BSP_LCD_SetFont(&Font16);
         BSP_LCD_DisplayStringAt(0,120, (uint8_t*)"NDEF write error!", CENTER_MODE);
        HAL_Delay(500);        
      }
      BSP_LCD_SetFont(&Font22);
    } // else {
      // return MENU_CALLBACK_CONTINUE;
      //}

  }
  return MENU_CALLBACK_CONTINUE;
}

/* Tag Tab setup */
static Menu_Tab_Setup_t tabSetup[] = {
{"Tag Info", &tabFirstTagReadInfo, &tabTagReadInfo},  /* Tag information Tab */
{"NDEF", &tabFirstTagReadNdef, &tabTagReadNdef},      /* Tag content Tab */
};

/* Helper function to setup BSP with correct NFC protocol */
static void SelectNDEFProtocol(uint8_t devId)
{  
    switch (gDevList[devId].type)
    {
      case BSP_NFCTAG_NFCA:
        switch (gDevList[devId].dev.nfca.type)
        {
          case RFAL_NFCA_T1T:
            NfcTag_SelectProtocol(NFCTAG_TYPE1);
          break;
          case RFAL_NFCA_T2T:
            NfcTag_SelectProtocol(NFCTAG_TYPE2);
          break;
          case RFAL_NFCA_T4T:
            NfcTag_SelectProtocol(NFCTAG_TYPE4);
          break;
        }
      break;
      case BSP_NFCTAG_NFCB:
        NfcTag_SelectProtocol(NFCTAG_TYPE4);
      break;
      case BSP_NFCTAG_ST25TB:
        NfcTag_SelectProtocol(NFCTAG_TYPE4);
      break;
      case BSP_NFCTAG_NFCF:
        NfcTag_SelectProtocol(NFCTAG_TYPE3);
      break;
      case BSP_NFCTAG_NFCV:
            NfcTag_SelectProtocol(NFCTAG_TYPE5);
      break;
      default:
        return;
    }

}

/* Helper function to manage user touchscreen interface */
static uint8_t manageInput(void)
{
  Menu_Position_t touch;

  if(Menu_ReadPosition(&touch))
  {
    if(touch.Y > 200)
    {
      // make sure the Wakeup mode is deactivated
      rfalWakeUpModeStop();
      // switch the field off before leaving the demo
      rfalFieldOff();
      return 1;
    } else if (instructionsDisplayed) {
      if(touch.Y < 60)
      {
        if(detectMode != DETECT_MODE_POLL)
        {
          detectMode = DETECT_MODE_POLL;
          rfalWakeUpModeStop();
          setRadio();
        }
      } else {
        if(detectMode != DETECT_MODE_WAKEUP)
        {
          detectMode = DETECT_MODE_WAKEUP;
          rfalFieldOff();                                                          /* Turns the Field On and starts GT timer */
          HAL_Delay(100);
          rfalWakeUpModeStart(NULL);
          setRadio();
        }
      }
    } else {
      uint8_t devId = 0xFF;
      // tags are in the field
      if((touch.Y < getTagBoxY(1)) && (gDevCnt >= 1))
      {
        devId = 0;
      } else if ((touch.Y < getTagBoxY(2)) && (gDevCnt >= 2))
      {
        devId = 1;
      } else if ((touch.Y < getTagBoxY(3)) && (gDevCnt >= 3))
      {
        devId = 2;
      } else if (gDevCnt >= 4){
        devId = 3;
      }
      if (devId != 0xFF)
      {  
        SelectNDEFProtocol(devId);
        gActiveDev = &gDevList[devId];

        /* Specific implementation for Random UIDs, as the selected UID has probably changed since last inventory */
        /* So re-run inventory and select the another random UID  */
        if((gActiveDev->type == BSP_NFCTAG_NFCA) &&
            (gActiveDev->dev.nfca.type == RFAL_NFCA_T4T) &&
            (gActiveDev->dev.nfca.nfcId1Len == 4) &&
            (gActiveDev->dev.nfca.nfcId1[0] == 0x08))
        {
          int err;
          uint8_t devCnt;
          // this is a random UID, need to refresh it
          rfalNfcaListenDevice nfcaDevList[EXAMPLE_RFAL_POLLER_DEVICES];
          
          rfalNfcaPollerInitialize();        
          rfalFieldOnAndStartGT();                                                          /* Turns the Field On and starts GT timer */
          err = rfalNfcaPollerFullCollisionResolution( RFAL_COMPLIANCE_MODE_NFC, EXAMPLE_RFAL_POLLER_DEVICES, nfcaDevList, &devCnt );
          if( (err == ERR_NONE) && (devCnt != 0) )
          {
            int searchIndex;
            for(searchIndex = 0; searchIndex < EXAMPLE_RFAL_POLLER_DEVICES ; searchIndex++ )
            {
              if( (nfcaDevList[searchIndex].type == RFAL_NFCA_T4T) &&
                  (nfcaDevList[searchIndex].nfcId1Len == 4) &&
                  (nfcaDevList[searchIndex].nfcId1[0] == 0x08))
              {
                // this is a random UID, select this one
                devId = 0;
                gDevList[devId].type     = BSP_NFCTAG_NFCA;
                gDevList[devId].dev.nfca = nfcaDevList[searchIndex];
                gActiveDev = &gDevList[devId];
                gDevCnt = 1;
                // stop searching random uid
                break;
              }
            }
          }
        }
        // be carefull this cast has not the same size!!!
        int status = BSP_NFCTAG_Activate((BSP_NFCTAG_Device_t *)&gDevList[devId]);

        Menu_MsgStatus("Tag read","",MSG_INFO);
        Menu_DisplayCenterString(10, "Touch here to exit");
        Menu_TabLoop(tabSetup,sizeof(tabSetup)/sizeof(Menu_Tab_Setup_t));

        BSP_LCD_SetFont(&Font22);
        Menu_SetStyle(CLEAR_PLAIN);
        Menu_FillRectangle(0,22,320,196);
        Menu_Delay(200);
        gActiveDev = NULL;
        gState = EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION;
      }
    }
  }
  return 0;
}

/** @brief Tag detection demo.
  *        Detect and display tags in the field.
  *        The user may select a detected tag to display tag details & content.
  */
void tagDetectionDemo (void)
{
  Menu_MsgStatus("Tag detection","",MSG_INFO);
  Menu_SetStyle(CLEAR_PLAIN);
  Menu_DisplayCenterString(10,"Touch here to exit");
  instructionsDisplayed = false;

  //Menu_SetStyle(PLAIN);
  detectMode = DETECT_MODE_POLL;
  tagDetectionNoTag();

  exampleRfalPollerRun();
}

/*!
 ******************************************************************************
 * \brief Passive Poller Run
 * 
 * This method implements the main state machine going thought all the 
 * different activities that a Reader/Poller device (PCD) needs to perform.
 * 
 * 
 ******************************************************************************
 */
static void exampleRfalPollerRun( void )
{
                                                            /* Initialize RFAL */
	platformLog("\n\rExample RFAL Poller started \r\n");
	
	for(;;)
	{
      rfalWorker();                                                                 /* Execute RFAL process */
	    if (detectMode == DETECT_MODE_WAKEUP)
      {
        if(!rfalWakeUpModeHasWoke())
        {
          if(manageInput())
          {
            return;
          }
          // still sleeping, don't do nothing...
          continue;
        }
        // exit wake up mode
        detectMode = DETECT_MODE_AWAKEN;
        rfalWakeUpModeStop();
      }

	    switch( gState )
	    {
	        /*******************************************************************************/
	        case EXAMPLE_RFAL_POLLER_STATE_INIT:                                     
	            
	            gTechsFound = EXAMPLE_RFAL_POLLER_FOUND_NONE; 
	            gActiveDev  = NULL;
	            gDevCnt     = 0;
	            
	            gState = EXAMPLE_RFAL_POLLER_STATE_TECHDETECT;
	            break;
	            
	            
            /*******************************************************************************/
	        case EXAMPLE_RFAL_POLLER_STATE_TECHDETECT:
	            
	            if( !exampleRfalPollerTechDetection() )                             /* Poll for nearby devices in different technologies */
	            {
	                gState = EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION;                  /* If no device was found, restart loop */
	                break;
	            }
	            
	            gState = EXAMPLE_RFAL_POLLER_STATE_COLAVOIDANCE;                      /* One or more devices found, go to Collision Avoidance */
	            break;
	            
	            
            /*******************************************************************************/
	        case EXAMPLE_RFAL_POLLER_STATE_COLAVOIDANCE:

              if(instructionsDisplayed)
              {
                for(int clearline = 1;clearline<9;clearline++)
                  BSP_LCD_ClearStringLine(clearline);
                instructionsDisplayed = false;
              }
              // add delay to avoid NFC-V Anticol frames back to back
	            if( !exampleRfalPollerCollResolution() )                              /* Resolve any eventual collision */
                {
                    gState = EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION;                  /* If Collision Resolution was unable to retrieve any device, restart loop */
                    break;
                }
	            
	            platformLog("Device(s) found: %d \r\n", gDevCnt);    
                
              gState = EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION;

                break;                
	            
            /*******************************************************************************/
	        case EXAMPLE_RFAL_POLLER_STATE_DEACTIVATION:
              if(!instructionsDisplayed)
              {
                BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
                BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
                for(int i = gDevCnt; i < 4; i++) 
                  displayTag(i, "", NULL);
                if(gDevCnt == 0)
                {
                  tagDetectionNoTag();
                  instructionsDisplayed = true;
                }
              }
	            exampleRfalPollerDeactivate();                                        /* If a card has been activated, properly deactivate the device */
              rfalFieldOff();                                                       /* Turn the Field Off powering down any device nearby */
              for(int delay = 0; delay < 100; delay++)
              {
                if(manageInput())
                {
                  return;
                }
                platformDelay( 1 );                                                 /* Remain a certain period with field off */
	            }
	            if((detectMode == DETECT_MODE_AWAKEN) && (gDevCnt == 0))
              {
                // no more tags, restart wakeup mode
                detectMode = DETECT_MODE_WAKEUP;
                rfalFieldOff();                                                          /* Turns the Field On and starts GT timer */
                HAL_Delay(100);
                rfalWakeUpModeStart(NULL);
                setRadio();
              }

	            gState = EXAMPLE_RFAL_POLLER_STATE_INIT;                              /* Restart the loop */
	            break;
	        
	        
            /*******************************************************************************/
	        default:
	            return;
	    }
      if(manageInput())
      {
        return;
      }
	}
}
	

/*!
 ******************************************************************************
 * \brief Poller Technology Detection
 * 
 * This method implements the Technology Detection / Poll for different 
 * device technologies.
 * 
 * \return true         : One or more devices have been detected
 * \return false         : No device have been detected
 * 
 ******************************************************************************
 */
static bool exampleRfalPollerTechDetection( void )
{
    ReturnCode           err;
    rfalNfcaSensRes      sensRes;
    rfalNfcbSensbRes     sensbRes;
    rfalNfcvInventoryRes invRes;
    uint8_t              sensbResLen;
    
    gTechsFound = EXAMPLE_RFAL_POLLER_FOUND_NONE;
    
    /*******************************************************************************/
    /* NFC-A Technology Detection                                                  */
    /*******************************************************************************/
    
    rfalNfcaPollerInitialize();                                                       /* Initialize RFAL for NFC-A */
    rfalFieldOnAndStartGT();                                                          /* Turns the Field On and starts GT timer */
    
    err = rfalNfcaPollerTechnologyDetection( RFAL_COMPLIANCE_MODE_NFC, &sensRes ); /* Poll for NFC-A devices */
    if( err == ERR_NONE )
    {
        gTechsFound |= EXAMPLE_RFAL_POLLER_FOUND_A;
    }
    
    
    /*******************************************************************************/
    /* NFC-B Technology Detection                                                  */
    /*******************************************************************************/
    
    rfalNfcbPollerInitialize();                                                       /* Initialize RFAL for NFC-B */
    rfalFieldOnAndStartGT();                                                          /* As field is already On only starts GT timer */
    
    err = rfalNfcbPollerTechnologyDetection( RFAL_COMPLIANCE_MODE_NFC, &sensbRes, &sensbResLen ); /* Poll for NFC-B devices */
    if( err == ERR_NONE )
    {
        gTechsFound |= EXAMPLE_RFAL_POLLER_FOUND_B;
    }
    
    /*******************************************************************************/
    /* ST25TB Technology Detection                                                  */
    /*******************************************************************************/
    
    rfalSt25tbPollerInitialize();                                                       /* Initialize RFAL for NFC-B */
    rfalFieldOnAndStartGT();  
    uint8_t          chipId;
                                                        /* As field is already On only starts GT timer */
    err = rfalSt25tbPollerCheckPresence( &chipId );
    if( err == ERR_NONE )
    {
        gTechsFound |= EXAMPLE_RFAL_POLLER_FOUND_ST25TB;
    }
    

    /*******************************************************************************/
    /* NFC-F Technology Detection                                                  */
    /*******************************************************************************/
    
    rfalNfcfPollerInitialize( RFAL_BR_212 );                                          /* Initialize RFAL for NFC-F */
    rfalFieldOnAndStartGT();                                                          /* As field is already On only starts GT timer */
    
    err = rfalNfcfPollerCheckPresence();                                              /* Poll for NFC-F devices */
    if( err == ERR_NONE )
    {
        gTechsFound |= EXAMPLE_RFAL_POLLER_FOUND_F;
    }
    
    
    /*******************************************************************************/
    /* NFC-V Technology Detection                                                  */
    /*******************************************************************************/
    
    rfalNfcvPollerInitialize();                                                       /* Initialize RFAL for NFC-V */
    rfalFieldOnAndStartGT();                                                          /* As field is already On only starts GT timer */
    
    err = rfalNfcvPollerCheckPresence( &invRes );                                     /* Poll for NFC-V devices */
    if( err == ERR_NONE )
    {
        gTechsFound |= EXAMPLE_RFAL_POLLER_FOUND_V;
    }
    
    return (gTechsFound != EXAMPLE_RFAL_POLLER_FOUND_NONE);
}

/*!
 ******************************************************************************
 * \brief Poller Collision Resolution
 * 
 * This method implements the Collision Resolution on all technologies that
 * have been detected before.
 * 
 * \return true         : One or more devices identified 
 * \return false        : No device have been identified
 * 
 ******************************************************************************
 */
static bool exampleRfalPollerCollResolution( void )
{
    uint8_t    i;
    uint8_t    devCnt;
    ReturnCode err;
    
    /*******************************************************************************/
    /* NFC-A Collision Resolution                                                  */
    /*******************************************************************************/
    if( gTechsFound & EXAMPLE_RFAL_POLLER_FOUND_A )                                   /* If a NFC-A device was found/detected, perform Collision Resolution */
    {
        rfalNfcaListenDevice nfcaDevList[EXAMPLE_RFAL_POLLER_DEVICES];
        
        rfalNfcaPollerInitialize();        
        err = rfalNfcaPollerFullCollisionResolution( RFAL_COMPLIANCE_MODE_NFC, (EXAMPLE_RFAL_POLLER_DEVICES - gDevCnt), nfcaDevList, &devCnt );
        if( (err == ERR_NONE) && (devCnt != 0) )
        {
            
            for( i=0; i<devCnt; i++ )                                                 /* Copy devices found form local Nfca list into global device list */
            {
                Menu_SetStyle(NFCA);
                displayTag(gDevCnt,"NFC-A",nfcaDevList[i].nfcId1);
                gDevList[gDevCnt].type     = BSP_NFCTAG_NFCA;
                gDevList[gDevCnt].dev.nfca = nfcaDevList[i];
                gDevCnt++;
            }
        }
    }
    
    /*******************************************************************************/
    /* NFC-B Collision Resolution                                                  */
    /*******************************************************************************/
    if( gTechsFound & EXAMPLE_RFAL_POLLER_FOUND_B )                                   /* If a NFC-A device was found/detected, perform Collision Resolution */
    {
        rfalNfcbListenDevice nfcbDevList[EXAMPLE_RFAL_POLLER_DEVICES];
        
        rfalNfcbPollerInitialize();        
        err = rfalNfcbPollerCollisionResolution( RFAL_COMPLIANCE_MODE_NFC, (EXAMPLE_RFAL_POLLER_DEVICES - gDevCnt), nfcbDevList, &devCnt );
        if( (err == ERR_NONE) && (devCnt != 0) )
        {
            for( i=0; i<devCnt; i++ )                                                 /* Copy devices found form local Nfcb list into global device list */
            {
                Menu_SetStyle(NFCB);
                displayTag(gDevCnt,"NFC-B",nfcbDevList[i].sensbRes.nfcid0);
                gDevList[gDevCnt].type     = BSP_NFCTAG_NFCB;
                gDevList[gDevCnt].dev.nfcb = nfcbDevList[i];
                gDevCnt++;
            }
        }
    }
    
    /*******************************************************************************/
    /* ST25TB Collision Resolution                                                  */
    /*******************************************************************************/
    if( gTechsFound & EXAMPLE_RFAL_POLLER_FOUND_ST25TB )                                   /* If a NFC-A device was found/detected, perform Collision Resolution */
    {
        rfalSt25tbListenDevice st25tbDevList[EXAMPLE_RFAL_POLLER_DEVICES];
        rfalSt25tbPollerInitialize();        
        err = rfalSt25tbPollerCollisionResolution((EXAMPLE_RFAL_POLLER_DEVICES - gDevCnt), st25tbDevList, &devCnt);
        if( (err == ERR_NONE) && (devCnt != 0) )
        {
            for( i=0; i<devCnt; i++ )                                                 /* Copy devices found form local Nfcb list into global device list */
            {
                Menu_SetStyle(NFCB);
                displayTag(gDevCnt,"ST25TB",st25tbDevList[i].UID);
                gDevList[gDevCnt].type     = BSP_NFCTAG_ST25TB;
                gDevList[gDevCnt].dev.st25tb = st25tbDevList[i];
                gDevCnt++;
            }
        }
    }
    
    /*******************************************************************************/
    /* NFC-F Collision Resolution                                                  */
    /*******************************************************************************/
    if( gTechsFound & EXAMPLE_RFAL_POLLER_FOUND_F )                                   /* If a NFC-F device was found/detected, perform Collision Resolution */
    {
        rfalNfcfListenDevice nfcfDevList[EXAMPLE_RFAL_POLLER_DEVICES];
        
        rfalNfcfPollerInitialize( RFAL_BR_212 );
        err = rfalNfcfPollerCollisionResolution( RFAL_COMPLIANCE_MODE_NFC, (EXAMPLE_RFAL_POLLER_DEVICES - gDevCnt), nfcfDevList, &devCnt );
        if( (err == ERR_NONE) && (devCnt != 0) )
        {
            for( i=0; i<devCnt; i++ )                                                 /* Copy devices found form local Nfcf list into global device list */
            {
                Menu_SetStyle(NFCF);
                displayTag(gDevCnt,"NFC-F",nfcfDevList[i].sensfRes.NFCID2);
                gDevList[gDevCnt].type     = BSP_NFCTAG_NFCF;
                gDevList[gDevCnt].dev.nfcf = nfcfDevList[i];
                gDevCnt++;
            }
        }
    }
    
    /*******************************************************************************/
    /* NFC-V Collision Resolution                                                  */
    /*******************************************************************************/
    if( gTechsFound & EXAMPLE_RFAL_POLLER_FOUND_V )                                   /* If a NFC-F device was found/detected, perform Collision Resolution */
    {
        rfalNfcvListenDevice nfcvDevList[EXAMPLE_RFAL_POLLER_DEVICES];
        
        rfalNfcvPollerInitialize();
        err = rfalNfcvPollerCollisionResolution( (EXAMPLE_RFAL_POLLER_DEVICES - gDevCnt), nfcvDevList, &devCnt);
        if( (err == ERR_NONE) && (devCnt != 0) )
        {
            for( i=0; i<devCnt; i++ )                                                /* Copy devices found form local Nfcf list into global device list */
            {
                Menu_SetStyle(NFCV);
                
                uint8_t uid[RFAL_NFCV_UID_LEN];
                for(int uid_i = 0; uid_i < RFAL_NFCV_UID_LEN; uid_i ++)
                  uid[uid_i] = nfcvDevList[i].InvRes.UID[RFAL_NFCV_UID_LEN - uid_i -1];
                displayTag(gDevCnt,"NFC-V",uid);
                gDevList[gDevCnt].type     = BSP_NFCTAG_NFCV;
                gDevList[gDevCnt].dev.nfcv = nfcvDevList[i];
                gDevCnt++;
            }
        }
    }
    
    return (gDevCnt > 0);
}


/*!
 ******************************************************************************
 * \brief Poller NFC DEP Deactivate
 * 
 * This method Deactivates the device if a deactivation procedure exists 
 * 
 * \return true         : Deactivation successful 
 * \return false        : Deactivation failed
 * 
 ******************************************************************************
 */
static bool exampleRfalPollerDeactivate( void )
{
    if( gActiveDev != NULL )                                                          /* Check if a device has been activated */
    {
        switch( gActiveDev->rfInterface )
        {
            /*******************************************************************************/
            case EXAMPLE_RFAL_POLLER_INTERFACE_RF:
                break;                                                                /* No specific deactivation to be performed */
                
            /*******************************************************************************/
            case EXAMPLE_RFAL_POLLER_INTERFACE_ISODEP:
                rfalIsoDepDeselect();                                                 /* Send a Deselect to device */
                break;
                
            /*******************************************************************************/
            case EXAMPLE_RFAL_POLLER_INTERFACE_NFCDEP:
                rfalNfcDepRLS();                                                      /* Send a Release to device */
                break;
                
            default:
                return false;
        }
        platformLog("Device deactivated \r\n");
    }
    
    return true;
}
