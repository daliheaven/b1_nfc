#ifndef _VERIFY_SIGNATURE_H_
#define _VERIFY_SIGNATURE_H_
#include <stdint.h>

int32_t ECDSA_VerifySignature(
  const uint8_t *InputMessage,
  uint32_t InputMessageLength,
  const uint8_t * sign_r,
  int32_t sign_r_size,
  const uint8_t * sign_s,
  int32_t sign_s_size,
  const uint8_t * pub_x,
  int32_t pub_x_size,
  const uint8_t * pub_y,
  int32_t pub_y_size
);

#endif
