#ifndef _TRUST25_H_
#define _TRUST25_H_

#include "stdint.h"
#include "rfal_rf.h"
#include "st25_discovery_st25r.h"

typedef enum {
  TRUST25_VALID,
  TRUST25_INVALID,
  TRUST25_NOT_TRUSTED
} TruST25_Status_t;

TruST25_Status_t TruST25_SignatureVerification(BSP_NFCTAG_Protocol_Id_t tagType, rfalNfcaListenDeviceType tag4Type, uint8_t* devUid, uint32_t nbUidDigit);

#endif
